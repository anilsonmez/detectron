# Copyright (c) 2017-present, Facebook, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
"""Provide stub objects that can act as stand-in "dummy" datasets for simple use
cases, like getting all classes in a dataset. This exists so that demos can be
run without requiring users to download/install datasets first.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from utils.collections import AttrDict


def get_coco_dataset():
    """A dummy COCO dataset that includes only the 'classes' field."""
    ds = AttrDict()
    classes = [
        '__background__', 'person', 'bicycle', 'car', 'motorcycle', 'airplane',
        'bus', 'train', 'truck', 'boat', 'traffic light', 'fire hydrant',
        'stop sign', 'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse',
        'sheep', 'cow', 'elephant', 'bear', 'zebra', 'giraffe', 'backpack',
        'umbrella', 'handbag', 'tie', 'suitcase', 'frisbee', 'skis',
        'snowboard', 'sports ball', 'kite', 'baseball bat', 'baseball glove',
        'skateboard', 'surfboard', 'tennis racket', 'bottle', 'wine glass',
        'cup', 'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple', 'sandwich',
        'orange', 'broccoli', 'carrot', 'hot dog', 'pizza', 'donut', 'cake',
        'chair', 'couch', 'potted plant', 'bed', 'dining table', 'toilet', 'tv',
        'laptop', 'mouse', 'remote', 'keyboard', 'cell phone', 'microwave',
        'oven', 'toaster', 'sink', 'refrigerator', 'book', 'clock', 'vase',
        'scissors', 'teddy bear', 'hair drier', 'toothbrush'
    ]
    ds.classes = {i: name for i, name in enumerate(classes)}
    return ds

def get_uav_dataset():
    """A dummy uav dataset that includes only the 'classes' field."""
    ds = AttrDict()
    classes = [
        '__background__', 'UFO', 'UAV'
    ]
    ds.classes = {i: name for i, name in enumerate(classes)}
    return ds

def get_uav_5_dataset():
    """A dummy uav_5 dataset that includes only the 'classes' field."""
    ds = AttrDict()
    classes = [
        '__background__', 'UFO', 'UAV', 'UFO1', 'UFO2', 'UAV2'
    ]
    ds.classes = {i: name for i, name in enumerate(classes)}
    return ds

def get_uav_1_dataset():
    """A dummy uav_1 dataset that includes only the 'classes' field."""
    ds = AttrDict()
    classes = [
        '__background__', 'UFO'
    ]
    ds.classes = {i: name for i, name in enumerate(classes)}
    return ds

def get_uav_16_dataset():
    """A dummy uav_16 dataset that includes only the 'classes' field."""
    ds = AttrDict()
    classes = [
        "__background__", "bird", "person", "car", "bicycle", "airplane",
        "bus", "uav", "cat", "dog", "ufo", "boat", "train",
        "truck", "motorcycle", "helicopter"
    ]
    ds.classes = {i: name for i, name in enumerate(classes)}
    return ds


def get_uav_18_dataset():
    """A dummy uav_18 dataset that includes only the 'classes' field."""
    ds = AttrDict()
    classes = [
        '__background__', 'uav', 'airplane', 'bicycle', 'bird', 'boat',
        'bus', 'car', 'cat', 'cow', 'dog', 'horse', 'motorcycle',
        'person', 'traffic_light', 'train', 'truck', 'ufo', 'helicopter'
    ]
    ds.classes = {i: name for i, name in enumerate(classes)}
    return ds

def get_uav_15_dataset():
    """A dummy uav_16 dataset that includes only the 'classes' field."""
    ds = AttrDict()
    ds.classes = {0: "__background__", 1: "uav", 2: "airplane", 3: "bicycle", 4: "bird",
                  5: "boat", 6: "bus", 7: "car", 8: "cat", 9: "undefined", 10: "dog", 11: "undefined", 12: "motorcycle",
                  13: "person", 14: "undefined", 15: "train", 16: "truck", 17: "ufo", 18: "helicopter"}
    return ds
