"""Perform inference on one or more datasets."""
import argparse
import cv2
import os
import pprint
import sys
import time
import torch
import onnx
import torch.onnx
import torchvision

import _init_paths  # pylint: disable=unused-import
from core.config import cfg, merge_cfg_from_file, merge_cfg_from_list, assert_and_infer_cfg
from core.test_engine import initialize_model_from_cfg

import utils.logging

# OpenCL may be enabled by default in OpenCV3; disable it because it's not
# thread safe and causes unwanted GPU memory allocations.
cv2.ocl.setUseOpenCL(False)


def parse_args():
    """Parse in command line arguments"""
    parser = argparse.ArgumentParser(description='Test a Fast R-CNN network')
    parser.add_argument(
        '--dataset',
        help='training dataset')
    parser.add_argument(
        '--dataset_name', dest='dataset_name', required=True,
        help='Dataset name to use')
    parser.add_argument(
        '--cfg', dest='cfg_file', required=True,
        help='optional config file')

    parser.add_argument(
        '--load_ckpt', help='path of checkpoint to load')
    parser.add_argument(
        '--load_detectron', help='path to the detectron weight pickle file')

    parser.add_argument(
        '--output_dir',
        help='output directory to save the testing results. If not provided, '
             'defaults to [args.load_ckpt|args.load_detectron]/../test.')

    parser.add_argument(
        '--set', dest='set_cfgs',
        help='set config keys, will overwrite config in the cfg_file.'
             ' See lib/core/config.py for all options',
        default=[], nargs='*')

    parser.add_argument(
        '--range',
        help='start (inclusive) and end (exclusive) indices',
        type=int, nargs=2)
    parser.add_argument(
        '--multi-gpu-testing', help='using multiple gpus for inference',
        action='store_true')
    parser.add_argument(
        '--vis', dest='vis', help='visualize detections', action='store_true')

    return parser.parse_args()


if __name__ == '__main__':

    if not torch.cuda.is_available():
        sys.exit("Need a CUDA device to run the code.")

    logger = utils.logging.setup_logging(__name__)
    args = parse_args()
    logger.info('Called with args:')
    logger.info(args)

    assert (torch.cuda.device_count() == 1) ^ bool(args.multi_gpu_testing)

    assert bool(args.load_ckpt) ^ bool(args.load_detectron), \
        'Exactly one of --load_ckpt and --load_detectron should be specified.'

    model = initialize_model_from_cfg(args)
    # Standard ImageNet input - 3 channels, 224x224,
    # values don't matter as we care about network structure.
    # But they can also be real inputs.
    dummy_input = Variable(torch.randn(1, 3, 224, 224))
    # Obtain your model, it can be also constructed in your script explicitly
    # model = torch.load("/media/anil/data_4TB/detectron/outputs/e2e_faster_rcnn_X-101-64x4d-FPN_1x/Apr17-20-16-10_uav_tiny_10K/ckpt/model_step199999.pth")

    # torchvision.models.alexnet(pretrained=True)
    # Invoke export
    torch.onnx.export(model, dummy_input, "test_detectron.onnx")

# def initialize_model_from_cfg(args, gpu_id=0):
#     """Initialize a model from the global cfg. Loads test-time weights and
#     set to evaluation mode.
#     """
#     model = model_builder.Generalized_RCNN()
#     model.eval()
#
#     if args.cuda:
#         model.cuda()
#
#     if args.load_ckpt:
#         load_name = args.load_ckpt
#         logger.info("loading checkpoint %s", load_name)
#         checkpoint = torch.load(load_name, map_location=lambda storage, loc: storage)
#         net_utils.load_ckpt(model, checkpoint['model'])
#
#     if args.load_detectron:
#         logger.info("loading detectron weights %s", args.load_detectron)
#         load_detectron_weight(model, args.load_detectron)
#
#     model = mynn.DataParallel(model, cpu_keywords=['im_info', 'roidb'], minibatch=True)
#
#     return model





